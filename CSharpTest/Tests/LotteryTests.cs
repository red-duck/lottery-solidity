using NUnit.Framework;
using System.Threading.Tasks;
using Nethereum.Web3;
using Nethereum.Web3.Accounts;
using System.Numerics;
using EthereumSmartContracts.Contracts.Lottery;
using EthereumSmartContracts.Contracts.Lottery.ContractDefinition;

namespace Tests
{
    [TestFixture]
    public class Tests
    {
        private static (string key, int amount, bool expected)[] accounts = new[]
        {
            ("0xdf57089febbacf7ba0bc227dafbffa9fc08a93fdc68e1e42411a14efcf23656e", 1000000000, true),
            ("0x59c6995e998f97a5a0044966f0945389dc9e86dae88c7a8412f4603b6b78690d", 1, true)
        };

        private readonly string url = "http://127.0.0.1:8545/";
        private readonly string expectedName = "Test";
        private BigInteger _expectedRegisteredTo;

        private LotteryService _service;
        private Account _owner;
        private string _contractAddress;
        
        [SetUp]
        public async Task Setup()
        {
            var privateKey = "0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80";
            _owner = new Account(privateKey);
            var web3 = new Web3(_owner, url);
        
            var deploymentMessage = new LotteryDeployment 
            {
                Name = expectedName,
                Duration = new BigInteger(1000000000),
            };
        
            var deploymentHandler = web3.Eth.GetContractDeploymentHandler<LotteryDeployment>();
            var transactionReceipt = await deploymentHandler.SendRequestAndWaitForReceiptAsync(deploymentMessage);
            _contractAddress = transactionReceipt.ContractAddress;
            _service = new LotteryService(new Web3(url), _contractAddress);
            _expectedRegisteredTo = await _service.RegisteredToQueryAsync();
        }
        
        [TestCase("Test")]
        public async Task Name_Should_Return_Expected_Value(string expected)
        {
            var value = await _service.NameQueryAsync();
        
            Assert.AreEqual(value, expected);
        }
        
        [TestCase("0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266")]
        public async Task Owner_Should_Return_Expected_Value(string expected) 
        {
            var value = await _service.OwnerQueryAsync();
            
            Assert.AreEqual(value, expected);
        }
        
        [Test]
        public async Task RegisteredTo_Should_Return_Expected_Value() 
        {
            var value = await _service.RegisteredToQueryAsync();
            var expected = _expectedRegisteredTo;
        
            Assert.AreEqual(value, expected);
        }
        
        [TestCase(0)]
        public async Task WinnerID_Should_Return_Expected_Value(int expectedValue) 
        {
            var value = await _service.WinnerIDQueryAsync();
            var expected = new BigInteger(expectedValue);
        
            Assert.AreEqual(value, expected);
        }

        [Test]
        public async Task Register_Should_Return_Expected()
        {
            foreach (var (key, amount, expected) in accounts)
            {
                var person = new Account(key);
                var web3 = new Web3(person, url);
                
                var registerHandler = web3.Eth.GetContractTransactionHandler<RegisterFunction>();
                var registerObject = new RegisterFunction
                {
                    AmountToSend = amount,
                    FromAddress = person.Address
                };
                var register = await registerHandler.SendRequestAndWaitForReceiptAsync(_contractAddress, registerObject);
                if(expected)
                    Assert.False(register.HasErrors());
                else
                    Assert.True(register.HasErrors());
            }
        }

        [Test]
        public async Task PlayLottery_Should_Return_Expected_Id()
        {
            foreach (var (key, amount, _) in accounts)
            {
                var person = new Account(key);
                var web31 = new Web3(person, url);
                var registerHandler = web31.Eth.GetContractTransactionHandler<RegisterFunction>();
                var registerObject = new RegisterFunction
                {
                    AmountToSend = amount,
                    FromAddress = person.Address
                };
                await registerHandler.SendRequestAndWaitForReceiptAsync(_contractAddress, registerObject);
            }
            var web3 = new Web3(_owner, url);   
            var playLotteryHandler = web3.Eth.GetContractQueryHandler<PlayLotteryFunction>();
            var playLottery = await playLotteryHandler.QueryAsync<BigInteger>(_contractAddress);
            Assert.AreEqual(new BigInteger(0), playLottery);
        }

        [Test]
        public async Task Claim_ShouldChangeBalance()
        {
            foreach (var (key, amount, _) in accounts)
            {
                var person = new Account(key);
                var web31 = new Web3(person, url);
                var registerHandler = web31.Eth.GetContractTransactionHandler<RegisterFunction>();
                var registerObject = new RegisterFunction
                {
                    AmountToSend = amount,
                    FromAddress = person.Address
                };
                await registerHandler.SendRequestAndWaitForReceiptAsync(_contractAddress, registerObject);
            }
            var web3 = new Web3(_owner, url);   
            
            var winner = 0;
            
            var winAccount = new Account(accounts[winner].key);
            var winWeb3 = new Web3(winAccount, url);
            var oldBalance = await winWeb3.Eth.GetBalance.SendRequestAsync(winAccount.Address);

            var playLotteryHandler = web3.Eth.GetContractQueryHandler<PlayLotteryFunction>();
            var playLottery = await playLotteryHandler.QueryAsync<BigInteger>(_contractAddress);

            var claimHandler = winWeb3.Eth.GetContractTransactionHandler<ClaimFunction>();
            var claimObject = new ClaimFunction
            {
                FromAddress = winAccount.Address
            };

            await claimHandler.SendRequestAndWaitForReceiptAsync(_contractAddress, claimObject);
            
            var newBalance = await winWeb3.Eth.GetBalance.SendRequestAsync(winAccount.Address);
            
            Assert.AreEqual(BigInteger.Parse("-276094999999999"), newBalance.Value - oldBalance.Value);
        }
    }
}