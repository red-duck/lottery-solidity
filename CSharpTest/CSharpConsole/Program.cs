﻿using System;
using System.Threading.Tasks;
using Nethereum.Web3;
using Nethereum.Web3.Accounts;
using System.Numerics;
using EthereumSmartContracts.Contracts.Lottery;
using EthereumSmartContracts.Contracts.Lottery.ContractDefinition;

namespace CSharpConsole
{
    internal static class Program
    {
        private static async Task Main(string[] args)
        {
            var url = "http://127.0.0.1:8545/"; 
            // var contractAddress = "0x5FbDB2315678afecb367f032d93F642f64180aa3";
            // var account = new Account(contractAddress);
            // var web3 = new Web3(url, contractAddress);
            // var service = new LotteryService(web3, contractAddress);
            //
            // var value = await service.WinnerIDQueryAsync();
            // Console.WriteLine(value);
            
            var privateKey = "0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80";
            var account = new Account(privateKey);
            var web3 = new Web3(account, url);
            
            var deploymentMessage = new LotteryDeployment 
            {
                Name = "Test",
                Duration = new BigInteger(1000000000),
            };
            
            var deploymentHandler = web3.Eth.GetContractDeploymentHandler<LotteryDeployment>();
            var transactionReceipt = await deploymentHandler.SendRequestAndWaitForReceiptAsync(deploymentMessage);
            var contractAddress = transactionReceipt.ContractAddress;
            
            var service = new LotteryService(new Web3(url), contractAddress);
            
            Console.WriteLine(account.Address);
            Console.WriteLine(await service.OwnerQueryAsync());
        }
    }
}
