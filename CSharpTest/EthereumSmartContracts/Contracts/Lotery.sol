pragma solidity >=0.4.19;

contract Lottery {
    struct Bet {
        uint from;
        uint to;
    }

    mapping (address => uint)   private  peopleIDs;
    Bet[]                       private  people;
    uint                        private  prize;
    address payable             public  owner;
    string                      public  name;
    uint                        public  registeredTo;
    uint                        public  winnerID;

    modifier onlyOwner() {
        require(msg.sender == owner, "not an owner");
        _;
    }

    constructor (string memory _name, uint _duration) {
        owner = payable(msg.sender);
        name = _name;
        registeredTo = block.timestamp * 1 days * _duration;
        winnerID = 0;
        prize = 0;
    }

    function register() public payable {
        require(peopleIDs[msg.sender] >= 0);

        prize += msg.value;
        peopleIDs[msg.sender] = people.length;
        
        if(people.length == 0) {
            people.push(Bet(0, msg.value - 1));
        } else {
            people.push(Bet(people[people.length - 1].to + 1, people[people.length - 1].to + msg.value));
        }

        emit registered(msg.sender);
    }

    function random() private view returns(uint) {
        return uint(keccak256(abi.encodePacked(block.difficulty, block.timestamp)));
    }

    function playLottery() onlyOwner public returns (uint id) {
        require(block.timestamp <= registeredTo, "No time");
        
        uint winnerPoint = random() % prize;

        for (uint i = 0; i < people.length; i++) {
            if ((winnerPoint >= people[i].from) && (winnerPoint <= people[i].to)) {
                winnerID = i;
            }
        }
        emit winnerFounded(winnerID);
        return winnerID;
    }

    function claim() public {
        require(peopleIDs[msg.sender] == winnerID, "U r not the winner");

        payable(msg.sender).transfer(prize);
    }

    function killContract() onlyOwner public {
        selfdestruct(owner);
    }

    event winnerFounded(uint _id);
    event registered(address _address);
}
