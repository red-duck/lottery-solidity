using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Numerics;
using Nethereum.Hex.HexTypes;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.Web3;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.Contracts.CQS;
using Nethereum.Contracts.ContractHandlers;
using Nethereum.Contracts;
using System.Threading;
using EthereumSmartContracts.Contracts.Lottery.ContractDefinition;

namespace EthereumSmartContracts.Contracts.Lottery
{
    public partial class LotteryService
    {
        public static Task<TransactionReceipt> DeployContractAndWaitForReceiptAsync(Nethereum.Web3.Web3 web3, LotteryDeployment lotteryDeployment, CancellationTokenSource cancellationTokenSource = null)
        {
            return web3.Eth.GetContractDeploymentHandler<LotteryDeployment>().SendRequestAndWaitForReceiptAsync(lotteryDeployment, cancellationTokenSource);
        }

        public static Task<string> DeployContractAsync(Nethereum.Web3.Web3 web3, LotteryDeployment lotteryDeployment)
        {
            return web3.Eth.GetContractDeploymentHandler<LotteryDeployment>().SendRequestAsync(lotteryDeployment);
        }

        public static async Task<LotteryService> DeployContractAndGetServiceAsync(Nethereum.Web3.Web3 web3, LotteryDeployment lotteryDeployment, CancellationTokenSource cancellationTokenSource = null)
        {
            var receipt = await DeployContractAndWaitForReceiptAsync(web3, lotteryDeployment, cancellationTokenSource);
            return new LotteryService(web3, receipt.ContractAddress);
        }

        protected Nethereum.Web3.Web3 Web3{ get; }

        public ContractHandler ContractHandler { get; }

        public LotteryService(Nethereum.Web3.Web3 web3, string contractAddress)
        {
            Web3 = web3;
            ContractHandler = web3.Eth.GetContractHandler(contractAddress);
        }

        public Task<string> ClaimRequestAsync(ClaimFunction claimFunction)
        {
             return ContractHandler.SendRequestAsync(claimFunction);
        }

        public Task<string> ClaimRequestAsync()
        {
             return ContractHandler.SendRequestAsync<ClaimFunction>();
        }

        public Task<TransactionReceipt> ClaimRequestAndWaitForReceiptAsync(ClaimFunction claimFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(claimFunction, cancellationToken);
        }

        public Task<TransactionReceipt> ClaimRequestAndWaitForReceiptAsync(CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync<ClaimFunction>(null, cancellationToken);
        }

        public Task<string> KillContractRequestAsync(KillContractFunction killContractFunction)
        {
             return ContractHandler.SendRequestAsync(killContractFunction);
        }

        public Task<string> KillContractRequestAsync()
        {
             return ContractHandler.SendRequestAsync<KillContractFunction>();
        }

        public Task<TransactionReceipt> KillContractRequestAndWaitForReceiptAsync(KillContractFunction killContractFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(killContractFunction, cancellationToken);
        }

        public Task<TransactionReceipt> KillContractRequestAndWaitForReceiptAsync(CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync<KillContractFunction>(null, cancellationToken);
        }

        public Task<string> NameQueryAsync(NameFunction nameFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<NameFunction, string>(nameFunction, blockParameter);
        }

        
        public Task<string> NameQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<NameFunction, string>(null, blockParameter);
        }

        public Task<string> OwnerQueryAsync(OwnerFunction ownerFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<OwnerFunction, string>(ownerFunction, blockParameter);
        }

        
        public Task<string> OwnerQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<OwnerFunction, string>(null, blockParameter);
        }

        public Task<string> PlayLotteryRequestAsync(PlayLotteryFunction playLotteryFunction)
        {
             return ContractHandler.SendRequestAsync(playLotteryFunction);
        }

        public Task<string> PlayLotteryRequestAsync()
        {
             return ContractHandler.SendRequestAsync<PlayLotteryFunction>();
        }

        public Task<TransactionReceipt> PlayLotteryRequestAndWaitForReceiptAsync(PlayLotteryFunction playLotteryFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(playLotteryFunction, cancellationToken);
        }

        public Task<TransactionReceipt> PlayLotteryRequestAndWaitForReceiptAsync(CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync<PlayLotteryFunction>(null, cancellationToken);
        }

        public Task<string> RegisterRequestAsync(RegisterFunction registerFunction)
        {
             return ContractHandler.SendRequestAsync(registerFunction);
        }

        public Task<string> RegisterRequestAsync()
        {
             return ContractHandler.SendRequestAsync<RegisterFunction>();
        }

        public Task<TransactionReceipt> RegisterRequestAndWaitForReceiptAsync(RegisterFunction registerFunction, CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync(registerFunction, cancellationToken);
        }

        public Task<TransactionReceipt> RegisterRequestAndWaitForReceiptAsync(CancellationTokenSource cancellationToken = null)
        {
             return ContractHandler.SendRequestAndWaitForReceiptAsync<RegisterFunction>(null, cancellationToken);
        }

        public Task<BigInteger> RegisteredToQueryAsync(RegisteredToFunction registeredToFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<RegisteredToFunction, BigInteger>(registeredToFunction, blockParameter);
        }

        
        public Task<BigInteger> RegisteredToQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<RegisteredToFunction, BigInteger>(null, blockParameter);
        }

        public Task<BigInteger> WinnerIDQueryAsync(WinnerIDFunction winnerIDFunction, BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<WinnerIDFunction, BigInteger>(winnerIDFunction, blockParameter);
        }

        
        public Task<BigInteger> WinnerIDQueryAsync(BlockParameter blockParameter = null)
        {
            return ContractHandler.QueryAsync<WinnerIDFunction, BigInteger>(null, blockParameter);
        }
    }
}
