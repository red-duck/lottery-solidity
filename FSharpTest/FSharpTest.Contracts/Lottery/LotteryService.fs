namespace FSharpTest.Contracts.Lottery

open System.Threading.Tasks
open System.Numerics
open Nethereum.Web3
open Nethereum.RPC.Eth.DTOs
open System.Threading
open FSharpTest.Contracts.Lottery.ContractDefinition


    type LotteryService (web3: Web3, contractAddress: string) =
    
        member val Web3 = web3 with get
        member val ContractHandler = web3.Eth.GetContractHandler(contractAddress) with get
    
        static member DeployContractAndWaitForReceiptAsync(web3: Web3, lotteryDeployment: LotteryDeployment, ?cancellationTokenSource : CancellationTokenSource): Task<TransactionReceipt> = 
            let cancellationTokenSourceVal = defaultArg cancellationTokenSource null
            web3.Eth.GetContractDeploymentHandler<LotteryDeployment>().SendRequestAndWaitForReceiptAsync(lotteryDeployment, cancellationTokenSourceVal)
        
        static member DeployContractAsync(web3: Web3, lotteryDeployment: LotteryDeployment): Task<string> =
            web3.Eth.GetContractDeploymentHandler<LotteryDeployment>().SendRequestAsync(lotteryDeployment)
        
        static member DeployContractAndGetServiceAsync(web3: Web3, lotteryDeployment: LotteryDeployment, ?cancellationTokenSource : CancellationTokenSource) = async {
            let cancellationTokenSourceVal = defaultArg cancellationTokenSource null
            let! receipt = LotteryService.DeployContractAndWaitForReceiptAsync(web3, lotteryDeployment, cancellationTokenSourceVal) |> Async.AwaitTask
            return new LotteryService(web3, receipt.ContractAddress);
            }
    
        member this.ClaimRequestAsync(claimFunction: ClaimFunction): Task<string> =
            this.ContractHandler.SendRequestAsync(claimFunction);
        
        member this.ClaimRequestAndWaitForReceiptAsync(claimFunction: ClaimFunction, ?cancellationTokenSource : CancellationTokenSource): Task<TransactionReceipt> =
            let cancellationTokenSourceVal = defaultArg cancellationTokenSource null
            this.ContractHandler.SendRequestAndWaitForReceiptAsync(claimFunction, cancellationTokenSourceVal);
        
        member this.KillContractRequestAsync(killContractFunction: KillContractFunction): Task<string> =
            this.ContractHandler.SendRequestAsync(killContractFunction);
        
        member this.KillContractRequestAndWaitForReceiptAsync(killContractFunction: KillContractFunction, ?cancellationTokenSource : CancellationTokenSource): Task<TransactionReceipt> =
            let cancellationTokenSourceVal = defaultArg cancellationTokenSource null
            this.ContractHandler.SendRequestAndWaitForReceiptAsync(killContractFunction, cancellationTokenSourceVal);
        
        member this.NameQueryAsync(nameFunction: NameFunction, ?blockParameter: BlockParameter): Task<string> =
            let blockParameterVal = defaultArg blockParameter null
            this.ContractHandler.QueryAsync<NameFunction, string>(nameFunction, blockParameterVal)
            
        member this.OwnerQueryAsync(ownerFunction: OwnerFunction, ?blockParameter: BlockParameter): Task<string> =
            let blockParameterVal = defaultArg blockParameter null
            this.ContractHandler.QueryAsync<OwnerFunction, string>(ownerFunction, blockParameterVal)
            
        member this.PlayLotteryRequestAsync(playLotteryFunction: PlayLotteryFunction): Task<string> =
            this.ContractHandler.SendRequestAsync(playLotteryFunction);
        
        member this.PlayLotteryRequestAndWaitForReceiptAsync(playLotteryFunction: PlayLotteryFunction, ?cancellationTokenSource : CancellationTokenSource): Task<TransactionReceipt> =
            let cancellationTokenSourceVal = defaultArg cancellationTokenSource null
            this.ContractHandler.SendRequestAndWaitForReceiptAsync(playLotteryFunction, cancellationTokenSourceVal);
        
        member this.RegisterRequestAsync(registerFunction: RegisterFunction): Task<string> =
            this.ContractHandler.SendRequestAsync(registerFunction);
        
        member this.RegisterRequestAndWaitForReceiptAsync(registerFunction: RegisterFunction, ?cancellationTokenSource : CancellationTokenSource): Task<TransactionReceipt> =
            let cancellationTokenSourceVal = defaultArg cancellationTokenSource null
            this.ContractHandler.SendRequestAndWaitForReceiptAsync(registerFunction, cancellationTokenSourceVal);
        
        member this.RegisteredToQueryAsync(registeredToFunction: RegisteredToFunction, ?blockParameter: BlockParameter): Task<BigInteger> =
            let blockParameterVal = defaultArg blockParameter null
            this.ContractHandler.QueryAsync<RegisteredToFunction, BigInteger>(registeredToFunction, blockParameterVal)
            
        member this.WinnerIDQueryAsync(winnerIDFunction: WinnerIDFunction, ?blockParameter: BlockParameter): Task<BigInteger> =
            let blockParameterVal = defaultArg blockParameter null
            this.ContractHandler.QueryAsync<WinnerIDFunction, BigInteger>(winnerIDFunction, blockParameterVal)
            
    

