module FSharpTest.Tests

open FSharpTest.Contracts.Lottery
open FSharpTest.Contracts.Lottery.ContractDefinition
open NUnit.Framework
open System
open Nethereum.Web3
open Nethereum.Web3.Accounts
open System.Numerics

let url = "http://127.0.0.1:8545/"
let ownerAccount = Account "0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80"
let ownerWeb3 = Web3 (ownerAccount, url)
let mutable contractAddress = ""
let mutable expectedRegisteredTo = 0I

let newService =
    let deploymentMessage = LotteryDeployment ()
    deploymentMessage.Name <- "Test"
    deploymentMessage.Duration <- 1000000000I
    
    let deploymentHandler = ownerWeb3.Eth.GetContractDeploymentHandler<LotteryDeployment>()
    let transactionReceipt = deploymentHandler.SendRequestAndWaitForReceiptAsync(deploymentMessage).Result
    
    contractAddress <- transactionReceipt.ContractAddress
    let service = LotteryService (Web3 url, contractAddress)
    
    let registeredToFunction = RegisteredToFunction ()
    registeredToFunction.FromAddress <- service.ContractHandler.ContractAddress
    
    expectedRegisteredTo <- (service.RegisteredToQueryAsync registeredToFunction).Result
    service

[<SetUp>]
let Setup () = ()

[<Test>]
[<TestCase ("Test", true)>]
[<TestCase ("Test1", false)>]
[<TestCase ("Test2", false)>]
let Name_Should_Return_Expected_Value (testName, expected) =
    let service = newService
    let nameFunction = NameFunction ()
    nameFunction.FromAddress <- service.ContractHandler.ContractAddress
    (testName = (service.NameQueryAsync nameFunction).Result) |> if expected then Assert.True else Assert.False
    
[<Test>]
[<TestCase ("0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266", true)>]
[<TestCase ("", false)>]
[<TestCase ("12312", false)>]
let Owner_Should_Return_Expected_Value (testOwner, expected) =
    let service = newService
    let ownerFunction = OwnerFunction ()
    ownerFunction.FromAddress <- service.ContractHandler.ContractAddress
    (testOwner = (service.OwnerQueryAsync ownerFunction).Result) |> if expected then Assert.True else Assert.False

[<Test>]
let RegisteredTo_Should_Return_Expected_Value () =
    let service = newService
    let registeredToFunction = RegisteredToFunction ()
    registeredToFunction.FromAddress <- service.ContractHandler.ContractAddress
    let testRegisteredTo = (service.RegisteredToQueryAsync registeredToFunction).Result
    Assert.AreEqual(expectedRegisteredTo, testRegisteredTo)
    
[<Test>]
[<TestCase ("0", true)>]
[<TestCase ("1", false)>]
[<TestCase ("2", false)>]
let WinnerID_Should_Return_Expected_Value (testID, expected) =
    let service = newService
    let winnerIdFunction = WinnerIDFunction ()
    winnerIdFunction.FromAddress <- service.ContractHandler.ContractAddress
    (BigInteger.Parse(testID) = (service.WinnerIDQueryAsync winnerIdFunction).Result)
    |> if expected then Assert.True else Assert.False
    
let registerPerson (key: string) amount =
        newService |> ignore
        let person = Account key
        let web3 = Web3 (person, url)
        let registerHandler = web3.Eth.GetContractTransactionHandler<RegisterFunction> ()
        let registerObj = RegisterFunction ()
        registerObj.FromAddress <- person.Address
        registerObj.AmountToSend <- amount
        registerHandler.SendRequestAndWaitForReceiptAsync(contractAddress, registerObj).Result
   
[<Test>]
[<TestCase ("0xdf57089febbacf7ba0bc227dafbffa9fc08a93fdc68e1e42411a14efcf23656e", 100000000, true)>]
[<TestCase ("0x59c6995e998f97a5a0044966f0945389dc9e86dae88c7a8412f4603b6b78690d", 1, true)>]
let Register_Should_Expect_Error (key, amount: int, expected) =
        let register = registerPerson key (amount |> BigInteger)
        register.Status.Value |> printfn "%A"
        (register.Status.Value = 0I)
        |> if expected then Assert.False else Assert.True
        
[<Test>]
let PlayLottery_Should_Return_Expected_ID () =
    newService |> ignore
    [
        ("0xdf57089febbacf7ba0bc227dafbffa9fc08a93fdc68e1e42411a14efcf23656e", 1000000000)
        ("0x59c6995e998f97a5a0044966f0945389dc9e86dae88c7a8412f4603b6b78690d", 1)
    ] |> List.map (fun (key, amount) -> registerPerson key (amount |> BigInteger))
    |> ignore
    let playLotteryHandler = ownerWeb3.Eth.GetContractQueryHandler<PlayLotteryFunction> ()
    let playLottery = playLotteryHandler.QueryAsync<BigInteger>(contractAddress).Result
    Assert.AreEqual(BigInteger.Parse("0"), playLottery)
    
[<Test>]
let Claim_Should_Change_Balance () =
    newService |> ignore
    let array = [
        ("0xdf57089febbacf7ba0bc227dafbffa9fc08a93fdc68e1e42411a14efcf23656e", 100000000)
        ("0x59c6995e998f97a5a0044966f0945389dc9e86dae88c7a8412f4603b6b78690d", 1)
    ]
    array |> List.map (fun (key, amount) -> registerPerson key (amount |> BigInteger)) |> ignore
    let winKey, _ = array.[0].Deconstruct ()
    printfn "%A" <| winKey
    let winAccount = Account winKey
    let winWeb3 = Web3 (winAccount, url)
    let oldBalance = winWeb3.Eth.GetBalance.SendRequestAsync(winAccount.Address).Result
    ownerWeb3.Eth.GetContractQueryHandler<PlayLotteryFunction>().QueryAsync<BigInteger>(contractAddress).Result |> ignore
    let claimObj = ClaimFunction ()
    claimObj.FromAddress <- winAccount.Address
    winWeb3.Eth.GetContractTransactionHandler<ClaimFunction>()
        .SendRequestAndWaitForReceiptAsync(contractAddress, claimObj).Result |> ignore
    let newBalance = winWeb3.Eth.GetBalance.SendRequestAsync(winAccount.Address).Result
    Assert.AreEqual(BigInteger.Parse("-276095899999999"), newBalance.Value - oldBalance.Value)
    